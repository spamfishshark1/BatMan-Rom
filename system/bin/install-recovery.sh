#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/11120000.ufs/by-name/RECOVERY:37761024:fe17f4c5df25b744f142e31c3bd6d37023fbd5bf; then
  applypatch EMMC:/dev/block/platform/11120000.ufs/by-name/BOOT:33202176:a9c8531ea822682cdc91962e8832f8e977c4fd87 EMMC:/dev/block/platform/11120000.ufs/by-name/RECOVERY fe17f4c5df25b744f142e31c3bd6d37023fbd5bf 37761024 a9c8531ea822682cdc91962e8832f8e977c4fd87:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
