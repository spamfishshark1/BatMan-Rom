#!/sbin/sh
# Written by Tkkg1994

getprop ro.boot.bootloader >> /tmp/BLmodel
ACTUAL_CSC=`cat /efs/imei/mps_code.dat`
ACTUAL_OMC=`cat /efs/imei/omcnw_code.dat`
SALES_CODE=`cat /system/omc/sales_code.dat`
sed -i -- "s/CSC=//g" /tmp/aroma/csc.prop
NEW_CSC=`cat /tmp/aroma/csc.prop`

if grep -q G93 /tmp/BLmodel; then
	mount /dev/block/platform/155a0000.ufs/by-name/SYSTEM /system
else
	mount /dev/block/platform/11120000.ufs/by-name/SYSTEM /system
fi

# Change CSC to right model
if grep -q G955 /tmp/BLmodel; then
	sed -i -- 's/G950/G955/g' /system/etc/security/audit_filter_table
	sed -i -- 's/G950/G955/g' /system/omc/CSCVersion.txt
	sed -i -- 's/G950/G955/g' /system/omc/SW_Configuration.xml
	sed -i -- 's/G950/G955/g' /system/info.extra
	find /system -type f -name 'omc*' | xargs sed -i 's/SM-G950F/SM-G955F/g'
else if grep -q G950 /tmp/BLmodel; then
	sed -i -- 's/G955/G950/g' /system/etc/security/audit_filter_table
	sed -i -- 's/G955/G950/g' /system/omc/CSCVersion.txt
	sed -i -- 's/G955/G950/g' /system/omc/SW_Configuration.xml
	sed -i -- 's/G955/G950/g' /system/info.extra
	find /system -type f -name 'omc*' | xargs sed -i 's/SM-G955F/SM-G950F/g'
else if grep -q G935 /tmp/BLmodel; then
	sed -i -- 's/G955/G935/g' /system/etc/security/audit_filter_table
	sed -i -- 's/G955/G935/g' /system/omc/CSCVersion.txt
	sed -i -- 's/G955/G935/g' /system/omc/SW_Configuration.xml
	sed -i -- 's/G955/G935/g' /system/info.extra
	find /system -type f -name 'omc*' | xargs sed -i 's/SM-G955F/SM-G935F/g'
else if grep -q G930 /tmp/BLmodel; then
	sed -i -- 's/G955/G930/g' /system/etc/security/audit_filter_table
	sed -i -- 's/G955/G930/g' /system/omc/CSCVersion.txt
	sed -i -- 's/G955/G930/g' /system/omc/SW_Configuration.xml
	sed -i -- 's/G955/G930/g' /system/info.extra
	find /system -type f -name 'omc*' | xargs sed -i 's/SM-G955F/SM-G930F/g'
else
	echo "Not a supported model, keep csc config default!"
fi
fi
fi
fi

# Change build.prop to right model
if grep -q G955 /tmp/BLmodel; then
	if grep -q G955F /tmp/BLmodel; then
		echo "Already a G955F model, nothing to change"
	fi
	if grep -q G955N /tmp/BLmodel; then
		sed -i -- 's/G955F/G955N/g' /system/build.prop
		echo "Changed to G955N"
	fi
else if grep -q G950 /tmp/BLmodel; then
	sed -i -- 's/dream2lte/dreamlte/g' /system/build.prop
	sed -i -- 's/ro.sf.lcd_density=420/ro.sf.lcd_density=480/g' /system/build.prop
	sed -i -- 's/ro.sf.init.lcd_density=560/ro.sf.init.lcd_density=640/g' /system/build.prop
	if grep -q G950F /tmp/BLmodel; then
		sed -i -- 's/G955F/G950F/g' /system/build.prop
		echo "Changed to G950F"
	fi
	if grep -q G950N /tmp/BLmodel; then
		sed -i -- 's/G955F/G950N/g' /system/build.prop
		echo "Changed to G950N"
	fi
else if grep -q G935 /tmp/BLmodel; then
	sed -i -- 's/dream2lte/hero2lte/g' /system/build.prop
	if grep -q G935F /tmp/BLmodel; then
		sed -i -- 's/G955F/G935F/g' /system/build.prop
		echo "Changed to G935F"
	fi
	if grep -q G935K /tmp/BLmodel; then
		sed -i -- 's/G955F/G935K/g' /system/build.prop
		echo "Changed to G935K"
	fi
	if grep -q G935L /tmp/BLmodel; then
		sed -i -- 's/G955F/G935L/g' /system/build.prop
		echo "Changed to G935L"
	fi
	if grep -q G935S /tmp/BLmodel; then
		sed -i -- 's/G955F/G935S/g' /system/build.prop
		echo "Changed to G935S"
	fi
	if grep -q G935W8 /tmp/BLmodel; then
		sed -i -- 's/G955F/G935W8/g' /system/build.prop
		echo "Already a G935W8 model, nothing to change"
	fi
else if grep -q G930 /tmp/BLmodel; then
	sed -i -- 's/dream2lte/herolte/g' /system/build.prop
	sed -i -- 's/ro.sf.lcd_density=420/ro.sf.lcd_density=480/g' /system/build.prop
	sed -i -- 's/ro.sf.init.lcd_density=560/ro.sf.init.lcd_density=640/g' /system/build.prop
	if grep -q G930F /tmp/BLmodel; then
		sed -i -- 's/G955F/G930F/g' /system/build.prop
		echo "Changed to G930F"
	fi
	if grep -q G930K /tmp/BLmodel; then
		sed -i -- 's/G955F/G930K/g' /system/build.prop
		echo "Changed to G930K"
	fi
	if grep -q G930L /tmp/BLmodel; then
		sed -i -- 's/G955F/G930L/g' /system/build.prop
		echo "Changed to G930L"
	fi
	if grep -q G930S /tmp/BLmodel; then
		sed -i -- 's/G955F/G930S/g' /system/build.prop
		echo "Changed to G930S"
	fi
	if grep -q G930W8 /tmp/BLmodel; then
		sed -i -- 's/G955F/G930W8/g' /system/build.prop
		echo "Changed to G930W8"
	fi
else
	echo "Not a supported model, keep build.prop default!"
fi
fi
fi
fi

if grep -q G93 /tmp/BLmodel; then
	sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
	rm -r /efs/imei/omcnw_code.dat
	cp -r /efs/imei/mps_code.dat /efs/imei/omcnw_code.dat
	chmod 0664 /efs/imei/omcnw_code.dat
	sed -i -- "s/$SALES_CODE/$NEW_CSC/g" /system/omc/sales_code.dat
else
	sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
	sed -i -- "s/$ACTUAL_OMC/$NEW_CSC/g" /efs/imei/omcnw_code.dat
	sed -i -- "s/$SALES_CODE/$NEW_CSC/g" /system/omc/sales_code.dat
fi

if grep -q G93 /tmp/BLmodel; then
	sed -i -- 's/8895/8890/g' /system/build.prop
	sed -i -- 's/abox/arizona/g' /system/build.prop
	sed -i -- 's/ro.security.vaultkeeper.feature=1/ro.security.vaultkeeper.feature=0/g' /system/build.prop
	sed -i -- 's/cortex-a53/cortex-a15/g' /system/build.prop
	sed -i -- 's/exynos-m2/exynos-m1/g' /system/build.prop
	sed -i -- 's/16/0/g' /system/etc/floating_feature.xml
	echo "ro.exynos.dss=1" >> /system/build.prop
fi

exit 10
